# -*- coding: utf-8 -*-

from slack.web.classes.blocks import *
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import datetime
from haksik import haksik
from config import *




app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

first_statement = -1
second_statement = -1
third_statement = -1
fourth_statement = -1


time_list = ["아침", "점심", "저녁", "하루"]

school = ['Seoul', 'Korea', 'Yonsei']
school_building = { 'Seoul' : ['학생회관', '대학원 기숙사 식당', '전망대 (3식당)', '자하연식당', '학부 기숙사 식당'],
                    'Korea': ['학생회관 자율식당', '안암학사 식당', '생활관 식당', '산학관 식당', '교우회관 학생식당'],
                    'Yonsei': ['의료원 종합관', '에비슨 의생명 연구센터', '한경관-교직원식당', '맛나샘', '의료원 제중관']}

URL_common = "https://bds.bablabs.com/restaurants/";

URL_building = [ ["NTUzMDI1?", "MjEzNTQ4MDI1?", "NTczMjQ5?", "NjEzNzIx?", "NTkzNDgx?"],
                 ["LTI0OTYzMTM2?", "LTI0Njg1Mjc5?", "LTI0NjgzMDMx?", "LTI0Njg0MTU2?", "LTI0NjgwNzc1?" ],
                 ["MjI0MjYwOTQ0?", "MjI0MzI0MTAw?", "LTI0NTk5MzEx?", "LTI0ODY4MjMx?", "MjI0MjkyNTIx?"]]

URL_school = [ "campus_id=spgIiBzSj0", "campus_id=tqbAESBISp", "campus_id=IDrA5MHp97"]


#URL 생성 함수
def make_URL(first, second):
    return URL_common + URL_building[first-1][second-1] + URL_school[first-1]

# 대학 선택 함수
def select_univ(text):
    number = text.split()[1].replace(' ','')[0]

    try :
        number = int(number)
    except ValueError :
        number = -1

    global first_statement
    global second_statement
    global third_statement
    global fourth_statement

    if first_statement == -1:
        first_statement = 0
        text_univ = "*Sky Food* 입니다.\n*SKY* 학생들의 식단을 통해 여러분도 똑똑해지세요! \n원하는 *대학교* 를 선택해주세요.\n\n*1*. *S* eoul National Univ. \n\n*2*. *K* orea Univ. \n\n*3*. *Y* onsei Univ. "

        block1 = SectionBlock(text=text_univ)
        block2 = ImageBlock(
            image_url="https://scontent-icn1-1.xx.fbcdn.net/v/t1.0-1/32731997_159717508204736_6705416106313515008_n.png?_nc_cat=106&_nc_oc=AQn7M_Wu53m8iYgz6NhHI9NXMObo9HMtgwS2F8CPhG_YSPOWqemhA194NsThJy5sL1k&_nc_ht=scontent-icn1-1.xx&oh=d88376833f84185c97eed5c5be2b82c1&oe=5DC5A3EF",
            alt_text="image failed"
        )
        blocks = [block2, block1]
        return blocks

    elif first_statement == 0:
        if (number == 0):
            first_statement = -1
            return select_univ('a ' + str(number))
        elif (number >= 1) & (number <= 3):
            first_statement = number
            return select_building(number)
        else:
            text_univ = "`잘못 입력하셨습니다!`\n\n*0.* 뒤로 가기 \n\n*1*. *S* eoul National Univ. \n\n*2*. *K* orea Univ.\n\n*3*. *Y* onsei Univ. "
            block1 = SectionBlock(text=text_univ)
            blocks = [block1]
            return blocks
    else:
        return select_building(number)

#결과 출력 함수
def print_crawl():
    global first_statement
    global second_statement
    global third_statement
    global fourth_statement
    global school
    global school_building
    global time_list
    '''
    모든 선택 완료, 조건에 따라 크롤링 함수 호출
    '''

    first = first_statement
    second = second_statement
    third = third_statement
    fourth = fourth_statement

    src = make_URL(first, second)
    menu2 = haksik(first, src, third, fourth)
    # menu2 = ['1번메뉴', '2번메뉴']
    dt2 = datetime.datetime.now()
    dt2 += datetime.timedelta(days=third_statement-1)

    blocks = []
    blocks.append(SectionBlock(text="*" + school[first_statement-1] + " Univ.*\n" + "*" + school_building[school[first_statement-1]][second_statement-1] + "* 식당의\n" + "*" + str(dt2.month) + "월 " + str(dt2.day) + "일* 의 " + "*" + time_list[fourth_statement-1] + "* 식단 입니다.\n"))

    for i in menu2:
        blocks.append(SectionBlock(text=i))

    first_statement = second_statement = third_statement = fourth_statement = -1

    return blocks

#시간대 선택 함수
def select_time(number):
    global first_statement
    global second_statement
    global third_statement
    global fourth_statement
    global school
    global school_building

    dt = datetime.datetime.now()
    dt = dt + datetime.timedelta(days=third_statement - 1)

    # 시간대 선택 메세지 최초 출력
    if fourth_statement == -1:
        fourth_statement = 0
        text_time = "*" + str(dt.month) + " / " + str(dt.day) + "* 를 선택하셨습니다.\n*시간대* 를 골라주세요.\n\n*0*. 뒤로 가기 \n\n*1*. 아침\n\n*2*. 점심\n\n*3*. 저녁\n\n*4*. 전체"
        block1 = SectionBlock(
            text=text_time
        )
        return [block1]

    elif fourth_statement == 0:
        if number == 0:
            third_statement = -1
            fourth_statement = -1
            return select_date(number)
        # 올바른 시간대 입력시
        elif (number >= 1) & (number <= 4):  # 날짜
            fourth_statement = number
            return print_crawl()

        # 잘못된 시간대 입력시
        else:
            text_time = "`잘못 입력하셨습니다!`\n*시간대* 를 골라주세요.\n\n*0*. 뒤로 가기 \n\n*1*. 아침\n\n*2*. 점심\n\n*3*. 저녁\n\n*4*. 전체"
            block1 = SectionBlock(
                text=text_time
            )
            return [block1]

    else:
        return print_crawl()

#날짜 선택 함수
def select_date(number):
    global first_statement
    global second_statement
    global third_statement
    global fourth_statement
    global school
    global school_building

    dt = datetime.datetime.now()
    # 올바른 식당 선택 후
    text_day = []
    # 날짜 선택 메세지 최초 출력
    if third_statement == -1:
        third_statement = 0
        text_day.append("*" + school[first_statement - 1] + " University* 의 *" + school_building[school[first_statement - 1]][
            second_statement - 1] + "* 을(를) 선택하셨습니다.\n*날짜* 를 골라주세요.\n\n*0*. 뒤로 가기 \n")
        for i in range(7):
            text_day.append("*" + str(i + 1) + "*. " + str(dt.month) + "/" + str(dt.day) + "\n")
            dt = dt + datetime.timedelta(days=1)
        text = '\n'.join(text_day)
        block1 = SectionBlock(
            text=text
        )
        return [block1]

    elif third_statement == 0:
        if number == 0:
            second_statement = -1
            third_statement = -1
            return select_building(number)
        # 올바른 날짜 선택시
        elif (number >= 1) & (number <= 7):
            third_statement = number
            return select_time(number)

        # 잘못된 날짜 선택시
        else:
            text_day.append("`잘못 입력하셨습니다!`")
            text_day.append("*" + school[first_statement - 1] + " University* 의 *" + school_building[school[first_statement - 1]]
            [second_statement - 1] + "* 을(를) 선택하셨습니다.\n*날짜* 를 골라주세요.\n\n*0*. 뒤로 가기 \n")
            for i in range(7):
                text_day.append(" *" + str(i + 1) + "*. " + str(dt.month) + "/" + str(dt.day) + "\n")
                dt = dt + datetime.timedelta(days=1)
            text = '\n'.join(text_day)
            block1 = SectionBlock(
                text=text
            )
            return [block1]

    else:
        return select_time(number)

# 식당 선택 함수
def select_building(number):
    global first_statement
    global second_statement
    global third_statement
    global fourth_statement
    global school
    global school_building
    # 올바른 대학 선택 후
    text_building = []
    # 식당 선택 안내 메세지 최초 출력
    if second_statement == -1:
        second_statement = 0
        text_building.append("*" + school[first_statement - 1] + " University* 를 선택하셨습니다."
                                                                 "\n*식당* 을 골라주세요.\n\n*0*. 뒤로 가기 \n")
        for i in range(len(school_building[school[first_statement - 1]])):
            text_building.append("*" + str(i + 1) + "*. " + school_building[school[first_statement - 1]][i] + "\n")
        text_b = '\n'.join(text_building)
        block_univ = []
        block_univ.append(ImageBlock(
            image_url="http://www.snu.ac.kr/images/common/img/img01_mark_ab0102.gif",
            alt_text="image failed"
        ))
        block_univ.append(ImageBlock(
            image_url="http://www.korea.ac.kr/mbshome/mbs/university/images/img/img_1_5_1_1_1.jpg",
            alt_text="image failed"
        ))
        block_univ.append(ImageBlock(
            image_url="https://www.yonsei.ac.kr/_res/sc/img/intro/img_symbol6.png",
            alt_text="image failed"
        ))
        block1 = SectionBlock(
            text=text_b
        )
        return [block_univ[first_statement - 1], block1]
    # 올바른 식당 선택 시
    elif second_statement == 0:
        if number == 0:
            first_statement = -1
            second_statement = -1
            return select_univ('a ' + str(number))
        elif (number >= 1) & (number <= len(school_building[school[first_statement - 1]])):
            second_statement = number
            return select_date(number)
        # 잘못된 식당 선택시
        else:
            text_building.append("`잘못 입력하셨습니다!`\n")
            text_building.append("*" + school[first_statement - 1] + " University* 를 선택하셨습니다.\n*식당* 을 골라주세요.\n\n*0*. 뒤로 가기 \n")
            for i in range(len(school_building[school[first_statement - 1]])):
                text_building.append("*" + str(i + 1) + "*. " + school_building[school[first_statement - 1]][i] + "\n")
            text_b = '\n'.join(text_building)
            block1 = SectionBlock(
                text=text_b
            )
            return [block1]
    else:
        return select_date(number)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    my_blocks = select_univ(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(my_blocks)
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)