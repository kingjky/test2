from seoul_univ import seoul_univ
from korea_univ import korea_univ
from yonsei_univ import yonsei_univ

def haksik(fir, src, thi, four):
    # 여기에 함수를 구현해봅시다.
    if fir == 1:
        return seoul_univ(src, thi, four)

    elif fir == 2:
        return korea_univ(src, thi, four)

    elif fir == 3:
        return yonsei_univ(src, thi, four)